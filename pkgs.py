from contaminate import (
    Script
)

def dnf_install(*pkgs : str):
    pstr = " ".join(pkgs)
    return Script(f"RUN rm -fr /var/cache/dnf && fakeroot dnf -y install {pstr} && dnf clean all")

# This image defaults to python3.6 :_(
# So we need pip to install into a virtual environment
# so that pip, python commands always reference
# python3.8.
setup_venv = Script("""RUN \
       python3.8 -m venv --system-site-packages {venv} \
        && echo 'VIRTUAL_ENV={venv}' >>~/.bashrc \
        && echo 'PATH="$VIRTUAL_ENV/bin:$PATH"' >>~/.bashrc \
        && VIRTUAL_ENV={venv} \
        && PATH="$VIRTUAL_ENV/bin:$PATH" \
        && pip install --no-cache-dir -U pip setuptools
        """.format(venv = "/usr/local"))

# Setup the container to run in user-mode, instead of as root.
usermode = Script("""
        RUN fakeroot addgroup -S {user} && fakeroot adduser -S {user} -G {user}
        USER {user}
        """.format(user = "user"))

def pip_install(*pkgs : str):
    """ Enter the virtual env. specified by bashrc
        and run pip install.
    """
    return Script("RUN bash --login -c 'pip install --no-cache-dir" \
                  + f" --no-input {' '.join(pkgs)}'")
