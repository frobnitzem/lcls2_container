from pathlib import Path

from contaminate import (
    sequence_rules,
    Script,
    Upload
)

from pkgs import dnf_install, setup_venv, pip_install

# Note: Our base image is Centos 8 Stream.
# https://centos.pkgs.org/ says we will get cmake-3.26.5-2.
#
# Lots of these dependencies come from
# [pillow](https://pillow.readthedocs.io/en/latest/installation.html)
sys_pkgs = """python38-devel python38-numpy python38-Cython
              python3-pip cmake git openblas-devel hdf5-devel
              libtiff-devel libjpeg-devel openjpeg2-devel zlib-devel
              freetype-devel lcms2-devel libwebp-devel tcl-devel tk-devel
              harfbuzz-devel fribidi-devel libraqm-devel libimagequant-devel
              libxcb-devel gtk3-devel""".split()

# Two packages listed at https://exafel.github.io/docs/psana2-cctbx-install
# do not build correctly:
# - libtiff dies during install with "numpy not found"
# - scikit-learn relies on weird numpy/scipy versions that don't compile
pip_pkgs     = """h5py wxpython pillow mock pytest jinja2 tabulate
                  tqdm orderedset procrunner""".split()

rules = {
    "venv":   dnf_install(*sys_pkgs).apply( setup_venv ),
    # scipy uses meson, which doesn't find openblas without openblas.pc
    "pip":    Upload("openblas.pc", "/usr/lib64/pkgconfig/") \
              .apply(pip_install("numpy", "scipy")) \
              .apply(pip_install(*pip_pkgs)),
    "lcls2":  dnf_install("rapidjson-devel", "libcurl-devel") \
              .apply(Upload("lcls2", "/tmp/lcls2")) \
              .apply(Upload("lcls2.sh", "/tmp/lcls2.sh")) \
              .apply(Script("RUN bash --login /tmp/lcls2.sh")),
}

C = sequence_rules(rules, "summit_base", "venv", "pip", "lcls2")
