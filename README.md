# PSANA2 Container Build Scripts

This project uses [Contaminate](https://code.ornl.gov/99R/contaminate)
to build docker images using podman scripts crafted in python.

## Usage

This is just a script, so install the requirements
with:

    pip install -r requirements.txt

Also, set up your `$HOME/.config/containers/storage.conf`
following instructions on the
[OLCF User Guide](https://docs.olcf.ornl.gov/software/containers_on_summit.html).

Then run the build process as using:

    python3 psana2.py

You should see build output and a series of json files
created after each superstep.

List built images using:

    podman image ls

Test run a container (e.g. `venv.2`) with:

    podman run --net host -it localhost/venv.2   

To save the final psana2 image to a singularity file, 

    podman save -o psana2.tar localhost/psana2
    singularity build --disable-cache psana2.sif docker-archive://psana2.tar

Note that `contaminate/container_env.py`
has code to write environment and LSF launch
scripts, but you have to call it manually at this
point.  We plan to make this easier in the near
future.
