#!/bin/bash

set -e

cd /tmp
#git clone https://github.com/slac-lcls/lcls2.git
cd lcls2

export CMAKE_PREFIX_PATH="$VIRTUAL_ENV:$CMAKE_PREFIX_PATH"
export CMAKE_INSTALL_PREFIX="$VIRTUAL_ENV"
export CMAKE_BUILD_TYPE=RelWithDebInfo

function cmake_build() {
    cd $1
    shift
    mkdir -p build
    cd build
    cmake $@ ..
    make -j 4 install
    cd ../..
}
function pip_build() {
    cd $1
    shift
    pip install --no-deps $@ .
    cd ..
}
cmake_build xtcdata
cmake_build psalg -DBUILD_SHMEM=OFF
pip_build psalg

#cmake_build psdaq
#pip_build psdaq

pip install wheel
export INSTDIR=$VIRTUAL_ENV
pip_build psana --no-build-isolation
